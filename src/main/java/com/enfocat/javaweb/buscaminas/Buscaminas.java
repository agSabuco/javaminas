

package com.enfocat.javaweb.buscaminas;

import java.util.Random;

public class Buscaminas {

    private static final int ANCHO = 10 ;
    private static final int ALTO = 6 ;
    private static final int MINAS = 5 ;

    private static Mina[][] campo = generaCampo(MINAS);
    public int status = 0;

    
    private static Mina[][] generaCampo(int minas){

        Mina[][] celdas = new Mina[ALTO][ANCHO];

        Random rnd = new Random();
        for (int y = 0; y<ALTO; y++){
            for (int x = 0; x<ANCHO; x++){
                Mina m = new Mina(false, x, y);
                celdas[y][x] = m;
            }
        }

        for (int m = 0; m<minas; m++){
            int rndX=rnd.nextInt(ANCHO);
            int rndY=rnd.nextInt(ALTO);
            celdas[rndY][rndX].setMina(true);
        }
        
        return celdas;
    }

    public static Mina[][] getCampo(){
        return Buscaminas.campo;
    }

    public static void reset(){
        Buscaminas.campo = generaCampo(MINAS);
    }

    public static void mostrar(){
        for (int y = 0; y<ALTO; y++){
            for (int x = 0; x<ANCHO; x++){
                Buscaminas.campo[y][x].setVisible(true);
            }
        }

    }

    public static int minasCercanas(int a, int b) {
        //muestra las en las celdas el numero de bombas subyacentes
        int compAlto = 0;
        int compAncho = 0;
        int minasEncontradas = 0;
        //si minasEncontradas es un 0 lanzarle un click
        try {
            for(int i = -1; i<2;i++){
                for(int j = -1; j<2; j++) {
                    compAlto = a + i;
                    compAncho = b + j;
                    if(compAlto < ALTO && compAlto >= 0 && compAncho < ANCHO  && compAncho >= 0) {
                        if(Buscaminas.campo[compAlto][compAncho].isMina()){
                            minasEncontradas++;
                        }
                    }
                }
            } 
        } catch (Exception e) {}
        
       
        return minasEncontradas;
        
    }


    public static void clicar(int x, int y){
        //Buscaminas.minasCercanas(y, x);
        int minas = 0;
        if(Buscaminas.campo[y][x].isMina()) {
            Buscaminas.mostrar();
        } else {
            if(!Buscaminas.campo[y][x].isMina() && Buscaminas.minasCercanas(y, x) > 0) {
                Buscaminas.campo[y][x].setVisible(true);
            } else {
                minas = Buscaminas.minasCercanas(y, x);
                Buscaminas.buscarCeldasLimpias(minas, y, x);
            }
        }    
        
    }

    public static void buscarCeldasLimpias(int numMinas, int c, int d) {
        int subCompAlto = 0;
        int subCompAncho = 0;
        
        for(int k = -1; k<2;k++){
            for(int l = -1; l<2; l++) {
                subCompAlto = c + k;
                subCompAncho = d + l;
                if(subCompAlto < ALTO && subCompAlto >= 0 && subCompAncho < ANCHO  && subCompAncho >= 0) {
                    if(Buscaminas.minasCercanas(subCompAlto, subCompAncho) == 0 &&
                    !Buscaminas.campo[subCompAlto][subCompAncho].isVisible()) {

                        Buscaminas.campo[subCompAlto][subCompAncho].setVisible(true);
                        Buscaminas.clicar(subCompAncho, subCompAlto); 
                        
                    } else {
                        Buscaminas.campo[subCompAlto][subCompAncho].setVisible(true);
                    }                   
                } 
            }
        }  
    }

    public static String htmlCampo(){

        StringBuilder sb = new StringBuilder();
        String iniDivFila = "<div class='fila'>";
        String finDiv = "</div>";
        
        sb.append("<div class='campo'>");
        for (int y = 0; y<ALTO; y++){
            sb.append(iniDivFila);
            for (int x = 0; x<ANCHO; x++){
                /* int b = Buscaminas.minasCercanas(y, x);
                String myStr = Integer.toString(b);
                sb.append(myStr); */
                sb.append(Buscaminas.campo[y][x].html());
            }
            sb.append(finDiv);
        }
        sb.append(finDiv);

        return sb.toString();

    }


}