package com.enfocat.javaweb.buscaminas;

public class Mina {
    
    private boolean visible;
    private int x;
    private int y;
    private boolean mina = false;

    public Mina(boolean mina, int x, int y){
        this.mina = mina;
        this.x=x;
        this.y=y;
        this.visible=false;
    }

    public String html(){
        //condición ? expr1 : expr2
        String css = (this.mina) ? "celda mina" : "celda";
        if(!this.visible) css+=" oculta";
        if(this.isMina()){
            return String.format("<div class='%s' data-x='%d' data-y='%d'><i class=\"fas fa-bomb\"></i></div>", css, this.x, this.y);
        } else {
            return String.format("<div class='%s' data-x='%d' data-y='%d'>%d</div>", css, this.x, this.y,Buscaminas.minasCercanas(this.y, this.x));
        }
    }
    


    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isMina() {
        return mina;
    }

    public void setMina(boolean mina) {
        this.mina = mina;
    }



    

}

